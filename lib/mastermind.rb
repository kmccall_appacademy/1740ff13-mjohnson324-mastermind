class Code
  def initialize(pegs)
    @pegs = pegs
  end

  attr_reader :pegs

  def self.random
    colors = "rgbyop".chars
    new_code = []
    4.times { new_code << colors.shuffle[0] }
    Code.new(new_code)
  end

  def self.parse(code)
    code.each_char do |letter|
      raise "Invalid Colors Present" unless "rgbyopRGBYOP".include?(letter)
    end
    Code.new(code.chars)
  end

  PEGS = {}

  def [](index)
    @pegs[index]
  end

  def ==(code)
    return false unless code.is_a? Code
    test_code = code.pegs.join
    actual_code = @pegs.join
    test_code.casecmp(actual_code) == 0
  end

  def near_matches(code)
    near_matches = 0
    code_check_one = code.dup.pegs
    code_check_two = @pegs.dup.sort

    code_check_one.sort.each_with_index do |peg, idx|
      if code[idx] != @pegs[idx]
        near_matches += 1 if code_check_two.include?(peg)
      end
      code_check_two.delete_at(idx)
    end
    near_matches
  end

  def exact_matches(code)
    matches = 0
    code.pegs.each_with_index do |peg, idx|
      matches += 1 if peg == @pegs[idx]
    end
    matches
  end
end

class Game
  def initialize(code = Code.random)
    @secret_code = code
  end

  attr_reader :secret_code

  def get_guess
    code = gets.chomp
    Code.parse(code)
  end

  def display_matches(code)
    puts "#{code.near_matches(code)} near matches, #{code.exact_matches(code)} exact matches"
  end
end
